Weather app was built using Visual Stuido 2015 using ASP.NET MVC.

To compile and run, open the zipped Microsoft Visual Studio Solution with Visual Studio. 
http://i.imgur.com/Qx0R2Yd.png

Once open in Visual Studio, select the browser you would like to use to test in the drop down menu. 
http://i.imgur.com/7q989IZ.png

Once the desired browser is chosen, click it to run. 

You should see the app run in the browser once local host is established
http://i.imgur.com/VzLhG4T.png

By Defualt, 3 cities from British Columbia will display , with a representation of their current weather conditions.

A small time-stamp in the lower left of the screen will display the DateTime of when the weather snapshot was
returned. 

As the screen is resized, the content adjusts in a responsive manner. 
http://i.imgur.com/xZpBNpQ.png

In the bottom right of the screen, the user can toggle the display temperatur between Celsius to Fahrenheit.
This setting persists between views. 

Entering a city name in the text box, and pressing search will display a long term forcast for the queried city. 
http://i.imgur.com/tABNSIs.png

This screen will also dynamically respond to diffrent screen/device sizes. 
http://i.imgur.com/goSKyiz.png

Clicking on either the "Weather" , "Home" , or the Menu list in the top right corner (only appears when narrow), 
will bring the user back to the main window.

The user can also see the long term forcast of one of the 3 default cities by clicking on the city name in the main 
home page.

Any non-recognized cities that are queried will return a near matching city, and any empty or null queries will 
display Vancouver's long term forecast by default.


Brendan Woods