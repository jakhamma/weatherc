﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Weather.Models;

namespace Weather.Controllers
{
    public class HomeController : Controller
    {
        public string weatherDesription;

        public async Task<ActionResult> Index()
        {
            //the timestamp to be displayed on UI screen
            ViewBag.CurrentTime = DateTime.Now;
            ViewBag.IsMetric = GlobalVariables.IsMetric;
          
            //as per instructions, display vancouver and two other B.C. cities
            CitiesModel cities = new CitiesModel();

            WeatherModel model1 = new WeatherModel("Vancouver");
            WeatherModel model2 = new WeatherModel("Burnaby");
            WeatherModel model3 = new WeatherModel("Whistler");

            cities.Add(model1);
            cities.Add(model2);
            cities.Add(model3);

            return View(cities);
        }

        public async Task<ActionResult> ToggleTempUnits()
        {
            //toggle the bool for celsius/far and redisplay cities
            GlobalVariables.IsMetric = !GlobalVariables.IsMetric;

            //the timestamp to be displayed on UI screen
            ViewBag.CurrentTime = DateTime.Now;
            ViewBag.IsMetric = GlobalVariables.IsMetric;

            //as per instructions, display vancouver and two other B.C. cities
            CitiesModel cities = new CitiesModel();

            WeatherModel model1 = new WeatherModel("Vancouver");
            WeatherModel model2 = new WeatherModel("Burnaby");
            WeatherModel model3 = new WeatherModel("Whistler");

            cities.Add(model1);
            cities.Add(model2);
            cities.Add(model3);

            return RedirectToAction("Index", "Home");
        }

    }
}

