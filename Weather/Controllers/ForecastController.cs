﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Weather.Models;

namespace Weather.Controllers
{/// <summary>
/// A class to managed contolling the Forecast model, and forecast view.
/// </summary>
    public class ForecastController : Controller
    {

        //display the long term forecast of one of the cities, if clicked.
        public ActionResult Index(int? id)
        {
            WeatherModel model;
            ViewBag.IsMetric = GlobalVariables.IsMetric;

            switch (id)
            {
                case 0:
                    model = new WeatherModel("Vancouver");
                    return View(model);     

                case 1:
                    model = new WeatherModel("Burnaby");
                    return View(model);
                    
                case 2:
                    model = new WeatherModel("Whistler");
                    return View(model);
   
                default:
                    return View();
            }
        }


        //display the forecast of a city, if searched. 
        //incorrect searches appear to return the nearest 
        //matching city code. 
        [HttpPost]
        public ActionResult Searching(string city)
        {
            ViewBag.IsMetric = GlobalVariables.IsMetric;

            if (city != null && city != "")
            {
                WeatherModel model = new WeatherModel(city);
                return View(model);
            }
            else
            {
                WeatherModel model = new WeatherModel("Vancouver");
                return View(model);
            }
        }

    }
}