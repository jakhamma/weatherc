﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Weather.Models
{
    public static class GlobalVariables
    {
        public static bool IsMetric { get; set; } = true;
    }
}