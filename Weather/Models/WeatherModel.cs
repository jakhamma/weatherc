﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace Weather.Models
{
    /// <summary>
    /// A class used to Model weather for a given city. All weather is queired via openweathermap.org
    /// </summary>
    public class WeatherModel
    {
        public string Weburl = "http://api.openweathermap.org/data/2.5/weather?q=vancouver&mode=xml&appid=a261d0b5f91d49be3dc6b16ab109b71b";
        public string Key = "&appid=a261d0b5f91d49be3dc6b16ab109b71b";
        public string DataMode = "&mode=xml";
        public string UnitType = "&units=metric";
        public string NumberOfDays = "&cnt=7";
        public string IconBaseUrl = "http://openweathermap.org/img/w/";

        public XmlDocument WeatherData;

        public string Description = "default description";
        public string City;
        public int Temperature;
        public List<string> LongTermDailyDescription;
        public List<int> LongTermDailyTemp;
        public List<DateTime> LongTermDates;
        public string IconUrl;


        public WeatherModel(string city)
        {
            if(GlobalVariables.IsMetric)
            {
                UnitType = "&units=metric";
            }
            else
            {
                UnitType = "&units=imperial";
            }
            this.City = city;
            SetData(city);
            SetDescription();
            SetTemperature();
            SetLongTermData(city);
            SetLongTermForecast();
            SetLongTermDates();
        }


        public void SetData(string city)
        {
            Weburl = "http://api.openweathermap.org/data/2.5/weather?q=" + city + DataMode + UnitType + Key;
            var xml = new WebClient().DownloadString(new Uri(Weburl));
            WeatherData = new XmlDocument();
            WeatherData.LoadXml(xml);
            SetIconUrl();
        }

        public void SetLongTermData(string city)
        {
            Weburl = "http://api.openweathermap.org/data/2.5/forecast/daily?q=" + city + DataMode + UnitType + NumberOfDays + Key;
            var xml = new WebClient().DownloadString(new Uri(Weburl));
            WeatherData = new XmlDocument();
            WeatherData.LoadXml(xml);
        }

        public void SetIconUrl()
        {
            string IconCode = "";
            IconCode = WeatherData.DocumentElement.SelectSingleNode("weather").Attributes["icon"].Value;
            IconUrl = IconBaseUrl + IconCode + ".png";
        }

        public void SetDescription()
        {
            Description = WeatherData.DocumentElement.SelectSingleNode("weather").Attributes["value"].Value;
        }


        public void SetTemperature()
        {
            Double temp;
            Double.TryParse(WeatherData.DocumentElement.SelectSingleNode("temperature").Attributes["value"].Value,out temp);
            Temperature = (int)temp;
        }

        public void SetLongTermForecast()
        {
            LongTermDailyDescription = new List<string>();
            LongTermDailyTemp = new List<int>();
            XmlNodeList elemList = WeatherData.GetElementsByTagName("symbol");

            for (int i = 0; i < elemList.Count; i++)
            {
                LongTermDailyDescription.Add(elemList[i].Attributes["name"].Value);
                
            }

            XmlNodeList elemList2 = WeatherData.GetElementsByTagName("temperature");
            for (int i = 0; i < elemList2.Count; i++)
            {
                int temp = 0;
                double tempDouble = 0.00;
                Double.TryParse(elemList2[i].Attributes["day"].Value,out tempDouble);
                temp = (int)tempDouble;
                LongTermDailyTemp.Add(temp);
            }
        }

        public void SetLongTermDates()
        {
            DateTime today = DateTime.Now;
            LongTermDates = new List<DateTime>();

            for (int i = 0; i < 7; i++)
            {
                LongTermDates.Add(today.AddDays(i));
            }
        }
    }
}